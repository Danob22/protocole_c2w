# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
from twisted.internet import  reactor
import struct
import time

class SibylServerTcpBinProtocol(Protocol):
	"""The class implementing the Sibyl TCP binary server protocol.
	
		.. note::
			You must not instantiate this class.  This is done by the code
			called by the main function.

		.. note::

			You have to implement this class.  You may add any attribute and
			method that you see fit to this class.  You must implement the
			following method (called by Twisted whenever it receives data):
			:py:meth:`~sibyl.main.protocol.sibyl_server_tcp_bin_protocol.dataReceived`
			See the corresponding documentation below.

	This class has the following attribute:

	.. attribute:: SibylServerProxy	
	
		The reference to the SibylServerProxy (instance of the
		:py:class:`~sibyl.main.sibyl_server_proxy.SibylServerProxy` class).

			.. warning::

				All interactions between the client protocol and the server
				*must* go through the SibylServerProxy.

	"""

	def __init__(self, sibylServerProxy):
		"""The implementation of the UDP server text protocol.

		Args:
			sibylServerProxy: the instance of the server proxy.
		"""
		self.sibylServerProxy = sibylServerProxy
		self.buffer = bytearray()        

	def dataReceived(self, line):
		"""Called by Twisted whenever a data is received

		Twisted calls this method whenever it has received at least one byte
		from the corresponding TCP connection.

		Args:
			line (bytes): the data received (can be of any length greater than
			one);
			
		.. warning::
			You must implement this method.  You must not change the parameters,
			as Twisted calls it.

		"""	
		"""# se desempaqueta el mensaje
		
		message_length = len(line) - 6       
		message = struct.unpack("!IH" + str(message_length) + "s" , line)	
		print(str(message[0]) + ": " + message[2].decode('utf-8'))
        
		# se empaqueta la respuesta
		reply = self.sibylServerProxy.generateResponse(message[2])
		length_reply = len(reply.encode('utf-8')) # len encode
		
		buf = bytearray(length_reply + 6)
		struct.pack_into("!IH" + str(length_reply) + 's', buf, 0, message[0], len(reply) + 6, reply.encode('utf-8'))
		
		self.transport.write(buf)
        
		print(buf)"""
			
		self.buffer = self.buffer + line
		#print(self.buffer)		
		
		while(len(self.buffer) >= 6):
			tiempo, message_length = struct.unpack_from("!IH" , self.buffer)		
			
			if len(self.buffer) < message_length:
				return
				
			message = struct.unpack_from("!" + str(message_length - 6) + "s" , self.buffer, 6)			
			message = message[0]
			reply = self.sibylServerProxy.generateResponse(message)
			length_reply = len(reply.encode('utf-8')) # len encode        
			buf = bytearray(length_reply + 6)
			#print(length_reply)
			struct.pack_into("!IH" + str(length_reply) + 's', buf, 0, tiempo, length_reply + 6, reply.encode('utf-8'))        
			self.transport.write(buf)
			self.buffer = self.buffer [message_length:]
			#print(self.buffer)


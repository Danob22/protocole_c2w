# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
import struct
import time


class SibylClientTcpBinProtocol(Protocol):
	"""
	The class implementing the Sibyl TCP binary client protocol.  It has
	the following attribute:

	.. attribute:: proxy

		The reference to the SibylCientProxy (instance of the
		:py:class:`~sibyl.main.sibyl_client_proxy.SibylClientProxy` class).

		.. warning::
			All interactions between the client protocol and the user
			interface *must* go through the SibylClientProxy.  In other
			words you must call one of the methods of
			:py:class:`~sibyl.main.sibyl_client_proxy.SibylClientProxy`
			whenever you would like the user interface to do something.
			
	.. note::
		You must not instantiate this class.  This is done by the code
		called by the main function.

	.. note::
		You have to implement this class.  You may add any attribute and
		method that you see fit to this class.  You must implement two
		methods:
		:py:meth:`~sibyl.main.protocol.sibyl_cliend_udp_text_protocol.sendRequest`
		and
		:py:meth:`~sibyl.main.protocol.sibyl_cliend_udp_text_protocol.dataReceived`.
		See the corresponding documentation below.
	"""

	def __init__(self, sibylProxy):
		"""The implementation of the UDP Text Protocol.

		Args:
			sibylClientProxy: the instance of the client proxy,
						this is the only way to interact with the user
						interface;
		"""
		self.clientProxy = sibylProxy
		self.buffer = bytearray() 
		self.flag = False

	def connectionMade(self):
		"""
		The Graphical User Interface (GUI) needs this function to know
		when to display the request window.

		DO NOT MODIFY IT.
		"""
		self.clientProxy.connectionSuccess()

	def sendRequest(self, line):
		"""Called by the controller to send the request

		The :py:class:`~sibyl.main.sibyl_client_proxy.SibylClientProxy` calls
		this method when the user clicks on the "Send Question" button.

		Args:
			line (string): the text of the question

		.. warning::
			You must implement this method.  You must not change the parameters,
			as the controller calls it.

		"""
		length_line = len(line.encode('utf-8'))
		buf = bytearray(length_line + 6)
		
		struct.pack_into("!IH"+ str(length_line)+ 's', buf, 0, round(time.time()), length_line + 6, line.encode('utf-8'))
		self.transport.write(buf)     
		
		#print(buf)    
		pass

	def dataReceived(self, line):
		"""Called by Twisted whenever a data is received

		Twisted calls this method whenever it has received at least one byte
		from the corresponding TCP connection.

		Args:
			line (bytes): the data received (can be of any length greater than
			one);

		.. warning::
			You must implement this method.  You must not change the parameters,
			as Twisted calls it.

		"""        
		self.buffer = self.buffer + line
		#print(self.buffer)		
		"""
		while(length_buffer >= 6 and self.flag):
			tiempo, message_length = struct.unpack("!IH" , self.buffer[0:6])		
			
			if message_length == length_buffer:
				message = struct.unpack("!" + str(message_length - 6) + "s" , self.buffer[6:])
				message = message[0]
				response = str(tiempo) + ": " + message.decode('utf-8')
				self.clientProxy.responseReceived(response)			
				print(response)
				self.flag = True				        
		#self.transport.loseConnection()
		"""
		
		while(len(self.buffer) >= 6):
			tiempo, message_length = struct.unpack_from("!IH" , self.buffer)
			if len(self.buffer) < message_length:
				return
				
			message = struct.unpack_from("!" + str(message_length - 6) + "s" , self.buffer, 6)
			response = message[0].decode('utf-8')
			self.clientProxy.responseReceived(response)			
			
			self.buffer = self.buffer [message_length:]	

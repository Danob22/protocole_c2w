# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
from twisted.internet import  reactor
import c2w.main.constants
import logging
import struct

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_server_protocol')


class c2wTcpChatServerProtocol(Protocol):

    def __init__(self, serverProxy, clientAddress, clientPort):
        """
        :param serverProxy: The serverProxy, which the protocol must use
            to interact with the user and movie store (i.e., the list of users
            and movies) in the server.
        :param clientAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param clientPort: The port number used by the c2w server,
            given by the user.

        Class implementing the TCP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: serverProxy

            The serverProxy, which the protocol must use
            to interact with the user and movie store in the server.

        .. attribute:: clientAddress

            The IP address of the client corresponding to this 
            protocol instance.

        .. attribute:: clientPort

            The port number used by the client corresponding to this 
            protocol instance.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.

        .. note::
            The IP address and port number of the client are provided
            only for the sake of completeness, you do not need to use
            them, as a TCP connection is already associated with only
            one client.
        """
        #: The IP address of the client corresponding to this 
        #: protocol instance.
        self.clientAddress = clientAddress
        #: The port number used by the client corresponding to this 
        #: protocol instance.
        self.clientPort = clientPort
        #: The serverProxy, which the protocol must use
        #: to interact with the user and movie store in the server.
        self.serverProxy = serverProxy
        self.num_sequence = 0 
        
        self.fiabiliteServer = {}
        
        self.buffer = bytearray()
        
        self.usersInMovie = {}
        for movie in self.serverProxy.getMovieList():
            self.usersInMovie[movie.movieTitle] = []

    def dataReceived(self, data):
        """
        :param data: The data received from the client (not necessarily
                     an entire message!)

        Twisted calls this method whenever new data is received on this
        connection.
        """
        self.buffer = self.buffer + data
        #int(self.buffer)		
		
        while(len(self.buffer) >= 4):
            buffer_length = len(self.buffer);
            header = struct.unpack_from('!I', self.buffer)
        
            #Del header toma typem, num_sequence, longueur
            typem, num_sequence, longueur = self.headerRead(header[0])       
            		
            if buffer_length < longueur:
                return
            
            if typem == 0:            
                self.fiabiliteServer[num_sequence][0] = True
                if self.fiabiliteServer[num_sequence][2] == 8:
                    for user in self.serverProxy.getUserList():
                        if user.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM or user.userChatRoom == None :
                            #print(user.userChatInstance)                        
                            user.userChatInstance.sendListUsers()
                elif self.fiabiliteServer[num_sequence][2] == 6:
                    user = self.serverProxy.getUserByAddress((self.clientAddress,self.clientPort))                   
                    if user.userChatRoom == None:
                        self.serverProxy.updateUserChatroom(user.userName, c2w.main.constants.ROOM_IDS.MAIN_ROOM)
                        #print(user)
                        #user.userChatInstance.sendListMovies()
                        self.sendListMovies()
            else:            
                self.sendAcquittement(num_sequence)
            
            message = struct.unpack_from("!" + str(longueur - 4) + "s" , self.buffer, 4)
            
            if (typem == 1) :                
                if(self.serverProxy.userExists(message[0].decode('utf-8')) == False):                                      
                    self.serverProxy.addUser(message[0].decode('utf-8'), None , userChatInstance= self , userAddress= (self.clientAddress,self.clientPort))
                    self.acceptationConnexion() 
                else: 
                    self.refusConnexion()

            if (typem == 2):
                # actualiza la situation actual del usuario
                film = struct.unpack_from('!' + str(longueur-4) + "s", self.buffer,4)[0].decode('utf-8')
                user = self.serverProxy.getUserByAddress((self.clientAddress,self.clientPort))            
                self.serverProxy.updateUserChatroom(user.userName, film)# c2w.main.constants.ROOM_IDS.MOVIE_ROOM)
                #print("listq de usuqrios", self.serverProxy.getUserList())
                for userf in self.serverProxy.getUserList():
                    if (userf.userChatRoom == film):
                        userf.userChatInstance.updateList(user.userName, film)
                        if userf.userName not in self.usersInMovie[film]:
                            self.usersInMovie[film].append(userf.userName)
                #print(self.serverProxy.getUserByAddress((self.clientAddress,self.clientPort)))
                self.serverProxy.startStreamingMovie(film)
                for u in self.serverProxy.getUserList():
                    if (u.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM):
                        u.userChatInstance.sendListUsers()
                    elif (u.userChatRoom == film):
                        u.userChatInstance.sendListUsersInFilm(film)
                        
            if (typem == 3):
                #print(self.buffer)
                user = self.serverProxy.getUserByAddress((self.clientAddress,self.clientPort))
                film = user.userChatRoom
                #if user.userName not in  self.usersInMovie[film] :
                self.usersInMovie[film].remove(user.userName) 
                for userf in self.serverProxy.getUserList():                    
                    userf.userChatInstance.updateListRemove(user.userName, film)                                      
                self.serverProxy.updateUserChatroom(user.userName, c2w.main.constants.ROOM_IDS.MAIN_ROOM)   
                #print("lista de usuarios: ", self.serverProxy.getUserList())
                #print("usuario: ", self.serverProxy.getUserByAddress((self.clientAddress,self.clientPort)))
                for u in self.serverProxy.getUserList():
                    #u.userChatInstance.usersInMovie = self.usersInMovie
                    print("****This is the movie list dictionary of:",u.userName,"  ==> ",u.userChatInstance.usersInMovie)
                    if (u.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM):
                        u.userChatInstance.sendListUsers()
                    elif (u.userChatRoom == film):
                        #print(u.userChatInstance.usersInMovie[film])
                        u.userChatInstance.sendListUsersInFilm(film)
            
            if (typem == 4):
                user = self.serverProxy.getUserByAddress((self.clientAddress,self.clientPort))
                self.serverProxy.removeUser(user.userName)
                #self.serverProxy.updateUserChatroom(user.userName, c2w.main.constants.ROOM_IDS.OUT_OF_THE_SYSTEM_ROOM) 
                for u in self.serverProxy.getUserList():
                    #u.userChatInstance.usersInMovie = self.usersInMovie
                    if (u.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM):
                        u.userChatInstance.sendListUsers()     
            
            if (typem == 7):            
                longeurPseudo = struct.unpack_from('!B', self.buffer, 4)
                pseudo = struct.unpack_from('!'+ str(longeurPseudo[0]) + 's', self.buffer, 5)            
                longeurMessage = longueur - 5 - longeurPseudo[0]           
                message = struct.unpack_from('!'+ str(longeurMessage) + 's',self.buffer, 5 + longeurPseudo[0])
                pseudo = pseudo[0].decode('utf-8')
                message = message[0].decode('utf-8')
                
                room = self.serverProxy.getUserByName(pseudo).userChatRoom            
                for user in self.serverProxy.getUserList():
                    if (user.userChatRoom == room and user.userName != pseudo ):
                        user.userChatInstance.sendChatMessageOIE(pseudo,message)
                        
            self.buffer = self.buffer [longueur:]
        pass
    
    #Generar la cabecera de cada mensaje aue se va enviar 
    def headerGenerate(self, typem, longueur, num_sequence = 0):
        #Convierte cada enteto typem, longueur, num_sequence a binario dependiendo del numero de bits definido para cada uno
        typeBin = '{0:04b}'.format(typem)
        num_sequenceBin = '{0:012b}'.format(self.num_sequence) if(typem != 0) else '{0:012b}'.format(num_sequence)
        longueurBin = '{0:016b}'.format(longueur)
        
        #se concatenan los tres valores binarios para convertirlos en entero
        header = (int(typeBin + num_sequenceBin + longueurBin, 2))
        
        #print('header bits', typeBin, num_sequenceBin, longueurBin)
        #print('header byte number', header)
        
        #se valida si type of message no es acquittement (0) para entonces incrementar el numero de secuencia
        if(typem != 0):
            self.fiabiliteServer[self.num_sequence] = [False,1,typem]
            self.num_sequence += 1        
        return header
    
        #Separa el header aue recibe, en sus componentes 
    def headerRead(self, header):
        #Convierte el enteto header a binario dependiendo del numero de bits definido para este
        headerBin = '{0:032b}'.format(header)
        
        #se separa la tupla de 32 bits en 4 bits de type, 12 bits de Num_sequence, 16 bits de longueur
        typem, num_sequence, longueur = (int(headerBin[0:4], 2), 
                               int(headerBin[4:16], 2), 
                               int(headerBin[16:32], 2))
        #se retornan los tres valores binarios        
        return typem, num_sequence, longueur
    
        #Se hace el envio del acquittement 
    def sendAcquittement(self, num_sequenceEmetteur):
        #Generar la cabecera para el mensaje de acquittement con type of message 0, 4 bytes a enviar y el numero de secuencia del emisor
        header = self.headerGenerate(0, 4, num_sequenceEmetteur)
        #El cliente envia un mensqje de acquittenment al servidor         
        buf = bytearray(4)
        struct.pack_into('!I', buf, 0, header)
        self.transport.write(buf)
        return
    
            #Se hace el envio del acquittement 
    def acceptationConnexion(self):
        #Generar la cabecera para el mensaje de acquittement con type of message 0, 4 bytes a enviar y el numero de secuencia del emisor
        header = self.headerGenerate(8, 4)
        #El cliente envia un mensqje de acquittenment al servidor         
        buf = bytearray(4)
        struct.pack_into('!I', buf, 0, header)
        self.transport.write(buf)
        reactor.callLater(1, self.resendMessage, buf)
        return
    
    def resendMessage(self, buf):        
        message_length = len(buf) - 4; 
        message = struct.unpack('!I' + str(message_length) + "s", buf)
        _, num_sequence, _ = self.headerRead(message[0]) 
        
        if (self.fiabiliteServer[num_sequence][0] != True):
            if self.fiabiliteServer[num_sequence][1] < 8:
                self.transport.write(buf)
                reactor.callLater(1, self.resendMessage, buf)
                self.fiabiliteServer[num_sequence][1] += 1
        return
    
    def sendListUsers(self):
        listAux = []
        count = 0
        listUsers = self.serverProxy.getUserList()
        for user in listUsers:
            #print("Usuario: ", user.userChatRoom) 
            if (user.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM or user.userChatRoom == None):
                userData = user.userName.encode('utf-8')
                listAux.append((userData, 0))
                count += len(userData) + 2
            else:
                userData = user.userName.encode('utf-8')
                listAux.append((userData, 1))
                count += len(userData) + 2
        header = self.headerGenerate(6, 4 + count)
        buf = struct.pack('!I',header)
        for userData, status in listAux: 
            buf += struct.pack('B' + str(len(userData)) + 'sB', len(userData), userData, status)
        self.transport.write(buf)
        reactor.callLater(1, self.resendMessage, buf)
        return
    
    def sendListMovies(self):
        listAux = []
        count = 0
        listMovies = self.serverProxy.getMovieList()
        for movie in listMovies:
            #print("Movie: ", movie.movieTitle)
            #print("IP: ", movie.movieIpAddress)
            #print("Port: ", movie.moviePort)
            movieData = movie.movieTitle
            intIP = movie.movieIpAddress.split('.')    
            #print(intIP)
            listAux.append((movieData.encode('utf-8'),int(intIP[0]),int(intIP[1]),int(intIP[2]),int(intIP[3]),movie.moviePort))
            count += len(movieData.encode('utf-8')) + 7 
        header = self.headerGenerate(5, 4 + count)
        buf = struct.pack('!I', header)
        for movieData, ip1, ip2, ip3, ip4, port in listAux:
            #print(movieData, ip1, ip2, ip3, ip4, port)
            buf += struct.pack('!B' + str(len(movieData)) + 'sBBBBH', len(movieData), movieData, ip1, ip2, ip3, ip4, port)
            #print(buf)
        self.transport.write(buf)
        reactor.callLater(1, self.resendMessage, buf)
        return
    
    def sendChatMessageOIE(self, pseudo, message):
        print(message) 
        print(pseudo)        
        messageHex = message.encode('utf-8')
        monNomHex = pseudo.encode('utf-8')
        
        header = self.headerGenerate(7, len(messageHex) + 5 + len(monNomHex))
        buf = struct.pack('!IB'+ str(len(monNomHex)) + 's' +str(len(messageHex))+ 's', header,len(monNomHex),monNomHex, messageHex)
        
        self.transport.write(buf)
        
        reactor.callLater(1, self.resendMessage, buf)
        
    def sendListUsersInFilm(self, film):
        listAux = []
        count = 0
        listUsers = self.usersInMovie[film]
        print(listUsers)
        for user in listUsers:
            #print("Usuario: ", user.userChatRoom)            
            userData = user.encode('utf-8')
            listAux.append((userData, 1))
            count += len(userData) + 2
            
        header = self.headerGenerate(6, 4 + count)
        buf = struct.pack('!I',header)
        for userData, status in listAux: 
            buf += struct.pack('B' + str(len(userData)) + 'sB', len(userData), userData, status)
        self.transport.write(buf)
        reactor.callLater(1, self.resendMessage, buf)
        return
    
    def updateList(self, userName, film):
        if userName not in  self.usersInMovie[film] :
            self.usersInMovie[film].append(userName)
        
    def updateListRemove(self, userName, film):
        if userName in self.usersInMovie[film]:
            self.usersInMovie[film].remove(userName)

    def refusConnexion(self):
        header = self.headerGenerate(9,4)
        buf = struct.pack('!I', header)        
        self.transport.write(buf)        
        reactor.callLater(1, self.resendMessage, buf)

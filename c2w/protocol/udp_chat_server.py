# -*- coding: utf-8 -*-
from twisted.internet.protocol import DatagramProtocol
from c2w.main.lossy_transport import LossyTransport
import c2w.main.constants
from twisted.internet import  reactor
import logging
import struct

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.udp_chat_server_protocol')


class c2wUdpChatServerProtocol(DatagramProtocol):

    def __init__(self, serverProxy, lossPr):
        """
        :param serverProxy: The serverProxy, which the protocol must use
            to interact with the user and movie store (i.e., the list of users
            and movies) in the server.
        :param lossPr: The packet loss probability for outgoing packets.  Do
            not modify this value!

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: serverProxy

            The serverProxy, which the protocol must use
            to interact with the user and movie store in the server.

        .. attribute:: lossPr

            The packet loss probability for outgoing packets.  Do
            not modify this value!  (It is used by startProtocol.)

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """
        #: The serverProxy, which the protocol must use
        #: to interact with the server (to access the movie list and to 
        #: access and modify the user list).
        self.serverProxy = serverProxy
        self.lossPr = lossPr
        
        # Numerotation des paquets pour controler les envois et receptions; cet sera initialise 0
        self.num_sequence = 0 
        
        self.fiabiliteServer = {}
        self.usersInMovie = {}
        self.messageRegistry = {}
        for movie in self.serverProxy.getMovieList():
            self.usersInMovie[movie.movieTitle] = []

    def startProtocol(self):
        """
        DO NOT MODIFY THE FIRST TWO LINES OF THIS METHOD!!

        If in doubt, do not add anything to this method.  Just ignore it.
        It is used to randomly drop outgoing packets if the -l
        command line option is used.
        """
        self.transport = LossyTransport(self.transport, self.lossPr)
        DatagramProtocol.transport = self.transport

    def datagramReceived(self, datagram, host_port):
        """
        :param string datagram: the payload of the UDP packet.
        :param host_port: a touple containing the source IP address and port.
        
        Twisted calls this method when the server has received a UDP
        packet.  You cannot change the signature of this method.
        """        
        #We unpack the message received from server
        message_length = len(datagram) - 4; 
        message = struct.unpack('!I' + str(message_length) + "s", datagram)
        
        #We obtain from header: type of message, sequence number and message length
        typem, num_sequence, longueur = self.headerRead(message[0])       
        print(datagram,'header number: ', typem, num_sequence, longueur)
        
        #We valid if type of message is acknowledgement
        if typem == 0:
            #if acknowledgement we change its status like received (status = true)            
            self.fiabiliteServer[num_sequence][0] = True
            #we execute the action corresponding to the type of message confirmed 
            if self.fiabiliteServer[num_sequence][2] == 8:                
                for user in self.serverProxy.getUserList():
                    if user.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM or user.userChatRoom == None :
                        print('aqui estoy')
                        self.sendListUsers(user.userAddress)
            elif self.fiabiliteServer[num_sequence][2] == 6:
                user = self.serverProxy.getUserByAddress(host_port)               
                if user.userChatRoom == None:
                    self.serverProxy.updateUserChatroom(user.userName, c2w.main.constants.ROOM_IDS.MAIN_ROOM)
                    print(user)
                    self.sendListMovies(host_port)
        else:
            #we send an acknowledgement
            self.sendAcquittement(num_sequence, host_port)
            if host_port not in self.messageRegistry:
                self.messageRegistry[host_port]=[]
            if (num_sequence,typem) in self.messageRegistry[host_port]:
                print("This message was already processed")
                return
            self.messageRegistry[host_port].append((num_sequence,typem))
            
            
        #We valid if type of message is to login. Number 1. We execute the action corresponding
        if (typem == 1) :
            #addUser(userName, userChatRoom, userChatInstance=None, userAddress=None)
            if(self.serverProxy.userExists(message[1].decode('utf-8')) == False):
                print(host_port)                                      
                self.serverProxy.addUser(message[1].decode('utf-8'), None , userChatInstance=None, userAddress=host_port)
                self.acceptationConnexion(host_port) 
            else: 
                self.refusConnexion(host_port)
                 
        #We valid if type of message is to select film. Number 2. We execute the action corresponding     
        if (typem == 2):
            # Update user status
            film = struct.unpack_from('!' + str(message_length) + "s", datagram,4)[0].decode('utf-8')
            user = self.serverProxy.getUserByAddress(host_port)            
            self.serverProxy.updateUserChatroom(user.userName, film)# c2w.main.constants.ROOM_IDS.MOVIE_ROOM)
            self.usersInMovie[film].append(user.userName)
            print(self.serverProxy.getUserByAddress(host_port))
            self.serverProxy.startStreamingMovie(film)
            for user in self.serverProxy.getUserList():
                if (user.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM):
                    self.sendListUsers(user.userAddress)
                elif (user.userChatRoom == film):
                    self.sendListUsersInFilm(user.userAddress,film)
                    
        #We valid if type of message is to leave of movie room. Number 3. We execute the action corresponding           
        if (typem == 3):
            print(datagram)
            user = self.serverProxy.getUserByAddress(host_port)
            film = user.userChatRoom
            self.usersInMovie[film].remove(user.userName)
            self.serverProxy.updateUserChatroom(user.userName, c2w.main.constants.ROOM_IDS.MAIN_ROOM)   
            print("lista de usuarios: ", self.serverProxy.getUserList())
            print("usuario: ", self.serverProxy.getUserByAddress(host_port))
            for u in self.serverProxy.getUserList():
                if (u.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM):
                    self.sendListUsers(u.userAddress)
                elif (u.userChatRoom == film):
                    print(self.usersInMovie[film])
                    self.sendListUsersInFilm(u.userAddress,film)
        
        #We valid if type of message is to leave of application. Number 4. We execute the action corresponding             
        if (typem == 4):
            user = self.serverProxy.getUserByAddress(host_port)
            self.serverProxy.removeUser(user.userName)
            #self.serverProxy.updateUserChatroom(user.userName, c2w.main.constants.ROOM_IDS.OUT_OF_THE_SYSTEM_ROOM) 
            for u in self.serverProxy.getUserList():
                if (u.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM):
                    self.sendListUsers(u.userAddress)                    
        
        #We valid if type of message is chat message. Number 7. We execute the action corresponding  
        if (typem == 7):            
            longeurPseudo = struct.unpack_from('!B', datagram, 4)
            pseudo = struct.unpack_from('!'+ str(longeurPseudo[0]) + 's', datagram, 5)            
            longeurMessage = longueur - 5 - longeurPseudo[0]           
            message = struct.unpack_from('!'+ str(longeurMessage) + 's', datagram, 5 + longeurPseudo[0])
            pseudo = pseudo[0].decode('utf-8')
            message = message[0].decode('utf-8')
            
            room = self.serverProxy.getUserByName(pseudo).userChatRoom            
            for user in self.serverProxy.getUserList():
                if (user.userChatRoom == room and user.userName != pseudo ):
                    self.sendChatMessageOIE(pseudo,message,user.userAddress)
        pass
    
    def headerGenerate(self, typem, longueur, num_sequence = 0):
        """
        :param integer typem: type of message.  
        :param integer longueur: message length including header + payload
        :param integer num_sequence: sequence number to differentiate each message of each client

        Called to generate the header message of any type of message.
        
        :returns a 4-byte integer that is generated with the three incoming parameters  
        """
        
        #We convert each integer (typem, longueur and num_sequence) to binary depending on bits number defined for each one.
        typeBin = '{0:04b}'.format(typem)
        num_sequenceBin = '{0:012b}'.format(self.num_sequence) if(typem != 0) else '{0:012b}'.format(num_sequence)
        longueurBin = '{0:016b}'.format(longueur)
        
        #We concatenate the three binary values to convert these to integer
        header = (int(typeBin + num_sequenceBin + longueurBin, 2))
     
        #We valide if type of message is not acquittement (0) by then to increase the sequence number
        if(typem != 0):
            self.fiabiliteServer[self.num_sequence] = [False, 1, typem]
            self.num_sequence += 1        
        return header
    
    def headerRead(self, header):
        """
        :param integer header: a 4-byte integer concerning with header.  

        Called to separate the header received in their components: type of message, sequence number, length.
        
        :returns three integers related to type of message, sequence number, length.  
        """           
        #We convert the header to binary depending on bit numbers defined for it 
        headerBin = '{0:032b}'.format(header)
        
        #We separate the 32 bits tuple in 4 bits of type of message, 12 bits of sequence number, 16 bits of message length  
        typem, num_sequence, longueur = (int(headerBin[0:4], 2), 
                               int(headerBin[4:16], 2), 
                               int(headerBin[16:32], 2))
       
        return typem, num_sequence, longueur
    
    #Se hace el envio del acquittement 
    def sendAcquittement(self, num_sequenceEmetteur, host_port):
        """
        :param integer num_sequenceEmetteur: the sequence number of message which will be acknowledged.
        :param host_port: a touple containing the source IP address and port.

        Called to send the acknowledgement
        """
        #We generate the header for acknowledge message
        header = self.headerGenerate(0, 4, num_sequenceEmetteur)
       
        buf = bytearray(4)
        struct.pack_into('!I', buf, 0, header)
        self.transport.write(buf, host_port)
        return
    
    def acceptationConnexion(self, host_port):        
        """
        :param host_port: a touple containing the source IP address and port.

        Called to accept an user connexion. 
        """
        #We send a type of message of number 8
        header = self.headerGenerate(8, 4)        
        buf = bytearray(4)
        struct.pack_into('!I', buf, 0, header)
        self.transport.write(buf, host_port)
        reactor.callLater(1, self.resendMessage, buf,host_port)
        return
    
    def resendMessage(self, buf,host_port):
        """
        :param bytearray buf: it contains the complete message (header + payload) which will be resent.
        :param host_port: a touple containing the source IP address and port.

        Called to resend a message which wasn't received, it means that wasn't acknowledged after waiting 1 second
        """ 
        #We obtain sequence number of message to know if this was already acknowledged        
        message_length = len(buf) - 4; 
        message = struct.unpack('!I' + str(message_length) + "s", buf)
        _, num_sequence, _ = self.headerRead(message[0]) 
        
        #If this message wasn't acknowledged, it will resend it until maximum 7 times
        if (self.fiabiliteServer[num_sequence][0] != True):
            if self.fiabiliteServer[num_sequence][1] < 8:
                self.transport.write(buf, host_port)
                reactor.callLater(1, self.resendMessage, buf, host_port)
                self.fiabiliteServer[num_sequence][1] += 1
        return
    
    def sendListUsers(self,host_port):
        """
        :param host_port: a touple containing the source IP address and port.

        Called to create and send user list. 
        """ 
        #We send a type of message of number 6 
        #We create a list to save users        
        listAux = []
        count = 0
        listUsers = self.serverProxy.getUserList()
        for user in listUsers:
            #We valid if a user is in home room or movie room
            if (user.userChatRoom == c2w.main.constants.ROOM_IDS.MAIN_ROOM or user.userChatRoom == None):
                userData = user.userName.encode('utf-8')
                listAux.append((userData, 0))#we assign 0 if user is in home room
                count += len(userData) + 2
            else:
                userData = user.userName.encode('utf-8')
                listAux.append((userData, 1))#we assign 1 if user is in home room
                count += len(userData) + 2
        header = self.headerGenerate(6, 4 + count)
        buf = struct.pack('!I',header)
        for userData, status in listAux: 
            buf += struct.pack('B' + str(len(userData)) + 'sB', len(userData), userData, status)
        self.transport.write(buf, host_port)
        reactor.callLater(1, self.resendMessage, buf,host_port)
        return
    
    def sendListUsersInFilm(self, host_port, film):
        """
        :param host_port: a touple containing the source IP address and port.
        :param film: film name.

        Called to create and send user list in a specific movie room. 
        """
        #We send a type of message of number 6
        listAux = []
        count = 0
        listUsers = self.usersInMovie[film]
        print(listUsers)
        for user in listUsers:          
            userData = user.encode('utf-8')
            listAux.append((userData, 1))
            count += len(userData) + 2
            
        header = self.headerGenerate(6, 4 + count)
        buf = struct.pack('!I',header)
        for userData, status in listAux: 
            buf += struct.pack('B' + str(len(userData)) + 'sB', len(userData), userData, status)
        self.transport.write(buf, host_port)
        reactor.callLater(1, self.resendMessage, buf,host_port)
        return
    
    def sendListMovies(self,host_port):
        """
        :param host_port: a touple containing the source IP address and port.

        Called to create and send movie list. 
        """
        #We send a type of message of number 5 
        #We create a list to save movies 
        listAux = []
        count = 0
        listMovies = self.serverProxy.getMovieList()
        for movie in listMovies:
            movieData = movie.movieTitle
            intIP = movie.movieIpAddress.split('.')
            listAux.append((movieData.encode('utf-8'),int(intIP[0]),int(intIP[1]),int(intIP[2]),int(intIP[3]),movie.moviePort))
            count += len(movieData.encode('utf-8')) + 7 
        header = self.headerGenerate(5, 4 + count)
        buf = struct.pack('!I', header)
        for movieData, ip1, ip2, ip3, ip4, port in listAux:            
            buf += struct.pack('!B' + str(len(movieData)) + 'sBBBBH', len(movieData), movieData, ip1, ip2, ip3, ip4, port)
        self.transport.write(buf, host_port)
        reactor.callLater(1, self.resendMessage, buf,host_port)
        return
    
    def sendChatMessageOIE(self, pseudo, message, host_port):
        """
        :param String pseudo: The user name that the user has typed.
        :param message: The text of the chat message.
        :param host_port: a touple containing the source IP address and port.

        Called to resend a chat message to all users.        
        """
        #We send a type of message of number 7
        messageHex = message.encode('utf-8')
        monNomHex = pseudo.encode('utf-8')
        
        header = self.headerGenerate(7, len(messageHex) + 5 + len(monNomHex))
        buf = struct.pack('!IB'+ str(len(monNomHex)) + 's' +str(len(messageHex))+ 's', header,len(monNomHex),monNomHex, messageHex)
        
        self.transport.write(buf, host_port)
        
        reactor.callLater(1, self.resendMessage, buf, host_port)
    
    def refusConnexion(self, host_port):
        """
        :param host_port: a touple containing the source IP address and port.

        Called to refuse an user connexion if his username is already exist. 
        """
        #We send a type of message of number 9
        header = self.headerGenerate(9,4)
        buf = struct.pack('!I', header)
        
        self.transport.write(buf, host_port)
        
        reactor.callLater(1, self.resendMessage, buf, host_port)
        
        
        
        
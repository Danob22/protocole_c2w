# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
from twisted.internet import  reactor
import c2w.main.constants
import logging
import struct


logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_client_protocol')


class c2wTcpChatClientProtocol(Protocol):

    def __init__(self, clientProxy, serverAddress, serverPort):
        """
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: serverAddress

            The IP address of the c2w server.

        .. attribute:: serverPort

            The port number used by the c2w server.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """

        #: The IP address of the c2w server.
        self.serverAddress = serverAddress
        #: The port number used by the c2w server.
        self.serverPort = serverPort
        #: The clientProxy, which the protocol must use
        #: to interact with the Graphical User Interface.
        self.clientProxy = clientProxy
        # Numerotation des paquets pour controler les envois et receptions; cet sera initialise 0
        self.num_sequence = 0 
        # 'NUMERO' : BOOL; intentos
        self.fiabiliteClient = {}
        self.userlist = []
        self.movielist = []
        self.buffer = bytearray()
        self.monNom = ''
        self.status = ''
        self.maMovie = ''
        
    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The client proxy calls this function when the user clicks on
        the login button.
        """
        moduleLogger.debug('loginRequest called with username=%s', userName)
        
        # para saber luego el largo real
        userNameHex = userName.encode('utf-8')
        # recuperamos los numeros de los headers ya manipulados
        header = self.headerGenerate(1, len(userNameHex) + 4)
        self.monNom = userName
        
        buf = bytearray(len(userNameHex) + 4)
        struct.pack_into('!I'+ str(len(userNameHex))+ 's', buf, 0, header, userNameHex)
        
        self.transport.write(buf)
        
        reactor.callLater(1, self.resendMessage, buf) 

    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string

        Called by the client proxy when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """
        print(message) 
        print(self.monNom)
        
        messageHex = message.encode('utf-8')
        monNomHex = self.monNom.encode('utf-8')
        
        header = self.headerGenerate(7, len(messageHex) + 5 + len(monNomHex))
        buf = struct.pack('!IB'+ str(len(monNomHex)) + 's' +str(len(messageHex))+ 's', header,len(monNomHex),monNomHex, messageHex)
        
        self.transport.write(buf)
        
        reactor.callLater(1, self.resendMessage, buf) 
        
        pass

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called by the client proxy  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """
        print(roomName)
        if self.status == 'A':
            self.status = "M"
            self.maMovie = roomName
            roomNameHex = roomName.encode('utf-8')
            header = self.headerGenerate(2, len(roomNameHex) + 4)        
            buf = bytearray(len(roomNameHex) + 4)        
            struct.pack_into('!I'+ str(len(roomNameHex))+ 's', buf, 0, header, roomNameHex)
        elif self.status == 'M':
            self.status = "A"
            self.maMovie = ''
            header = self.headerGenerate(3, 4)
            buf = bytearray(4)        
            struct.pack_into('!I', buf, 0, header)       
        
        self.transport.write(buf)        
        
        reactor.callLater(1, self.resendMessage, buf) 
        pass

    def sendLeaveSystemRequestOIE(self):
        """
        Called by the client proxy  when the user
        has clicked on the leave button in the main room.
        """
        header = self.headerGenerate(4, 4)
        buf = bytearray(4)        
        struct.pack_into('!I', buf, 0, header) 
        self.transport.write(buf)     
        reactor.callLater(1, self.resendMessage, buf) 
        pass

    def dataReceived(self, data):
        """
        :param data: The data received from the client (not necessarily
                     an entire message!)

        Twisted calls this method whenever new data is received on this
        connection.
        """
        self.buffer = self.buffer + data
        #int(self.buffer)		
		
        while(len(self.buffer) >= 4):
            buffer_length = len(self.buffer);
            header = struct.unpack_from('!I', self.buffer)
        
            #Del header toma typem, num_sequence, longueur
            typem, num_sequence, longueur = self.headerRead(header[0])       
            		
            if buffer_length < longueur:
                return
            
            if typem != 0:
                self.sendAcquittement(num_sequence)
            else: 
                self.fiabiliteClient[num_sequence][0] = True
                if self.fiabiliteClient[num_sequence][2] in [2,3]:                
                    self.clientProxy.joinRoomOKONE()
                if self.fiabiliteClient[num_sequence][2] == 4:                
                    #self.fiabiliteClient = {}
                    self.userlist = []
                    self.movielist = []
                    self.monNom = '' 
                    self.status = ''
                    self.maMovie = ''
                    self.clientProxy.leaveSystemOKONE()    
            
            if typem == 5:
                self.movieListRead(self.buffer)
                self.status = "A"
                #print(self.userlist)
                #print(self.movielist)
                #print("Llegue: ", self.monNom)
                self.clientProxy.initCompleteONE(self.userlist,self.movielist) 
            if typem == 6:
                self.userListRead(self.buffer)
                print("se recibio una lista con status " + self.status)
                print(self.userlist)
                if self.status == "A":
                    self.clientProxy.setUserListONE(self.userlist)
                elif self.status == "M":
                    print("hola listuser: ", self.userlist)
                    self.clientProxy.setUserListONE([])
                    for user in self.userlist:
                        self.clientProxy.userUpdateReceivedONE(user[0], self.maMovie)
                        
            if typem == 7:
                longeurPseudo = struct.unpack_from('!B', self.buffer, 4)
                pseudo = struct.unpack_from('!'+ str(longeurPseudo[0]) + 's', self.buffer, 5)            
                longeurMessage = longueur - 5 - longeurPseudo[0]           
                message = struct.unpack_from('!'+ str(longeurMessage) + 's', self.buffer, 5 + longeurPseudo[0])
                pseudo = pseudo[0].decode('utf-8')
                message = message[0].decode('utf-8')            
                self.clientProxy.chatMessageReceivedONE(pseudo, message)
                
            if typem == 9:
                self.clientProxy.connectionRejectedONE("User name is already used, please try again!")
                #self.__init__(self.serverAddress, self.serverPort, self.clientProxy, self.lossPr)
            # Limpio mi bufer
            self.buffer = self.buffer [longueur:]
        pass
    
        #Generar la cabecera de cada mensaje aue se va enviar 
    def headerGenerate(self, typem, longueur, num_sequence = 0):
        #Convierte cada enteto typem, longueur, num_sequence a binario dependiendo del numero de bits definido para cada uno
        typeBin = '{0:04b}'.format(typem)
        num_sequenceBin = '{0:012b}'.format(self.num_sequence) if(typem != 0) else '{0:012b}'.format(num_sequence)
        longueurBin = '{0:016b}'.format(longueur)
        
        #se concatenan los tres valores binarios para convertirlos en entero 
        header = (int(typeBin + num_sequenceBin + longueurBin, 2))
        
        #print('header bits', typeBin, num_sequenceBin, longueurBin)
        #print('header byte number', header)
        
        #se valida si type of message no es acquittement (0) para entonces incrementar el numero de secuencia
        if(typem != 0):
            self.fiabiliteClient[self.num_sequence] = [False,1,typem]            
            self.num_sequence += 1            
        #se retorna el header como entero
        return header
    
    def resendMessage(self, buf):
        
        message_length = len(buf) - 4; 
        message = struct.unpack('!I' + str(message_length) + "s", buf)
        _, num_sequence, _ = self.headerRead(message[0]) 
        if (self.fiabiliteClient[num_sequence][0] != True):
            if self.fiabiliteClient[num_sequence][1] < 8:
                self.transport.write(buf)
                reactor.callLater(1, self.resendMessage, buf)
                self.fiabiliteClient[num_sequence][1] += 1 
        return
    
    def sendAcquittement(self, num_sequenceEmetteur):
        #Generar la cabecera para el mensaje de acquittement 
        header = self.headerGenerate(0, 4, num_sequenceEmetteur)
        #El cliente envia un mensqje de acquittenment al servidor        
        buf = bytearray(4)
        struct.pack_into('!I', buf, 0, header)
        self.transport.write(buf)
        return
    
        #Separa el header aue recibe, en sus componentes 
    def headerRead(self, header):
        #Convierte el enteto header a binario dependiendo del numero de bits definido para este
        headerBin = '{0:032b}'.format(header)
        
        #se separa la tupla de 32 bits en 4 bits de type, 12 bits de Num_sequence, 16 bits de longueur
        typem, num_sequence, longueur = (int(headerBin[0:4], 2), 
                               int(headerBin[4:16], 2), 
                               int(headerBin[16:32], 2))
        #se retornan los tres valores binarios
        return typem, num_sequence, longueur
    
    def userListRead(self,datagram):
        listAux = []         
        header = struct.unpack_from('!I', datagram)
        _, _, maxLongueur = self.headerRead(header[0])
        count = 4
        print(maxLongueur)
        while (count != maxLongueur):            
            headerUser = struct.unpack_from('!B', datagram, count)            
            userData = struct.unpack_from('!' + str(headerUser[0]) + 'sB', datagram, count + 1)            
            count = count + headerUser[0] + 2
            userName = userData[0].decode('utf-8')
            if (userData[1] == 0):
                listAux.append((userName, c2w.main.constants.ROOM_IDS.MAIN_ROOM))
            elif(userData[1] == 1):
                listAux.append((userName, c2w.main.constants.ROOM_IDS.MOVIE_ROOM))           
        self.userlist = listAux
        return
    
    def movieListRead(self,datagram):
        listAux = [] 
        header = struct.unpack_from('!I', datagram)
        _, _, maxLongueur = self.headerRead(header[0])
        count = 4
        print(maxLongueur)
        while (count != maxLongueur):            
            headermovie = struct.unpack_from('!B', datagram, count)            
            Data = struct.unpack_from('!' + str(headermovie[0]) + 'sBBBBH', datagram, count + 1) 
            titleMovie = Data[0].decode('utf-8')
            listAux.append((titleMovie, str(Data[1]) + '.' + str(Data[2]) + '.' + str(Data[3]) + '.' + str(Data[4]), Data[5]))                      
            count = count + headermovie[0] + 7
        self.movielist = listAux       
        return
# -*- coding: utf-8 -*-
from twisted.internet.protocol import DatagramProtocol
from c2w.main.lossy_transport import LossyTransport
from twisted.internet import  reactor
import c2w.main.constants
import logging
import struct

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.udp_chat_client_protocol')


class c2wUdpChatClientProtocol(DatagramProtocol):

    def __init__(self, serverAddress, serverPort, clientProxy, lossPr):
        """
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attributes:

        .. attribute:: serverAddress

            The IP address of the c2w server.

        .. attribute:: serverPort

            The port number of the c2w server.

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: lossPr

            The packet loss probability for outgoing packets.  Do
            not modify this value!  (It is used by startProtocol.)

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """

        #: The IP address of the c2w server.
        self.serverAddress = serverAddress
        #: The port number of the c2w server.
        self.serverPort = serverPort
        #: The clientProxy, which the protocol must use
        #: to interact with the Graphical User Interface.
        self.clientProxy = clientProxy
        self.lossPr = lossPr
        
        # Numerotation des paquets pour controler les envois et receptions; cet sera initialise 0
        self.num_sequence = 0 
        # 'NUMERO' : BOOL; intentos
        self.fiabiliteClient = {}
        self.userlist = []
        self.movielist = []
        self.monNom = '' 
        self.status = ''
        self.maMovie = ''
        self.messageRegistry = []

    def startProtocol(self):
        """
        DO NOT MODIFY THE FIRST TWO LINES OF THIS METHOD!!

        If in doubt, do not add anything to this method.  Just ignore it.
        It is used to randomly drop outgoing packets if the -l
        command line option is used.
        """
        self.transport = LossyTransport(self.transport, self.lossPr)
        DatagramProtocol.transport = self.transport

    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The client proxy calls this function when the user clicks on
        the login button.
        """
        moduleLogger.debug('loginRequest called with username=%s', userName)        
        # To know what is the real value of username in bytes
        userNameHex = userName.encode('utf-8')
        # We send a type of message of number 1 
        header = self.headerGenerate(1, len(userNameHex) + 4)
        self.monNom = userName
        
        buf = bytearray(len(userNameHex) + 4)
        struct.pack_into('!I'+ str(len(userNameHex))+ 's', buf, 0, header, userNameHex)
        
        self.transport.write(buf,(self.serverAddress, self.serverPort))
        
        reactor.callLater(1, self.resendMessage, buf,(self.serverAddress, self.serverPort)) 
        
        
    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string

        Called by the client proxy  when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """
        #We encode message and username to know the real length in bytes of each one
        messageHex = message.encode('utf-8')
        monNomHex = self.monNom.encode('utf-8')
        #We send a type of message of number 7 
        header = self.headerGenerate(7, len(messageHex) + 5 + len(monNomHex))
        buf = struct.pack('!IB'+ str(len(monNomHex)) + 's' +str(len(messageHex))+ 's', header,len(monNomHex),monNomHex, messageHex)
        
        self.transport.write(buf,(self.serverAddress, self.serverPort))
        
        reactor.callLater(1, self.resendMessage, buf,(self.serverAddress, self.serverPort)) 
        
        pass

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called by the client proxy  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """
        #We consider two type of status: 'A' for Home Room and 'M' for Movie Room 
        if self.status == 'A':
            #if client is in home room, he will be sent a type of message of number 2 to server, to join to a movie room
            self.status = "M"
            self.maMovie = roomName
            roomNameHex = roomName.encode('utf-8')
            header = self.headerGenerate(2, len(roomNameHex) + 4)        
            buf = bytearray(len(roomNameHex) + 4)        
            struct.pack_into('!I'+ str(len(roomNameHex))+ 's', buf, 0, header, roomNameHex)
        elif self.status == 'M':
            #if client is in movie room, he will be sent a type of message of number 3 to server, to join to home room again
            self.status = "A"
            self.maMovie = ''
            header = self.headerGenerate(3, 4)
            buf = bytearray(4)        
            struct.pack_into('!I', buf, 0, header)       
        
        self.transport.write(buf,(self.serverAddress, self.serverPort))        
        
        reactor.callLater(1, self.resendMessage, buf,(self.serverAddress, self.serverPort)) 
        
        pass

    def sendLeaveSystemRequestOIE(self):
        """
        Called by the client proxy  when the user
        has clicked on the leave button in the main room.
        """
        #We send a type of message of number 4
        header = self.headerGenerate(4, 4)
        buf = bytearray(4)        
        struct.pack_into('!I', buf, 0, header) 
        self.transport.write(buf,(self.serverAddress, self.serverPort))     
        reactor.callLater(1, self.resendMessage, buf,(self.serverAddress, self.serverPort))         
        pass

    def datagramReceived(self, datagram, host_port):
        """
        :param string datagram: the payload and header of the UDP packet.
        :param host_port: a touple containing the source IP address and port.

        Called **by Twisted** when the client has received a UDP
        packet.
        """
        #We unpack the message received from server
        message_length = len(datagram) - 4; 
        message = struct.unpack('!I' + str(message_length) + "s", datagram)
        
        #We obtain from header: type of message, sequence number and message length  
        typem, num_sequence, longueur = self.headerRead(message[0])       
        print(message, 'header number: ', typem, num_sequence, longueur)
               
        #We valid if type of message is acknowledgement
        if typem == 0:
            #if acknowledgement we change its status like received (status = true)
            self.fiabiliteClient[num_sequence][0] = True
            #we execute the action corresponding to the type of message confirmed            
            if self.fiabiliteClient[num_sequence][2] in [2,3]:                
                self.clientProxy.joinRoomOKONE()
            if self.fiabiliteClient[num_sequence][2] == 4:                
                self.fiabiliteClient = {}
                self.userlist = []
                self.movielist = []
                self.monNom = '' 
                self.status = ''
                self.maMovie = ''
                self.clientProxy.leaveSystemOKONE()                
        else:
            #we send an acknowledgement
            self.sendAcquittement(num_sequence, host_port)
            if (num_sequence,typem) in self.messageRegistry:
                print("This message was already processed")
                return
            self.messageRegistry.append((num_sequence,typem))
        
        #We valid if type of message is movie list. Number 5. We execute the action corresponding
        if typem == 5:
            self.movieListRead(datagram)
            self.status = "A"
            self.clientProxy.initCompleteONE(self.userlist,self.movielist) 
            
        #We valid if type of message is user list. Number 6. We execute the action corresponding          
        if typem == 6:             
            self.userListRead(datagram) 
            if self.status == "A":
                self.clientProxy.setUserListONE(self.userlist)
            elif self.status == "M":
                self.clientProxy.setUserListONE([])
                for user in self.userlist:
                    self.clientProxy.userUpdateReceivedONE(user[0], self.maMovie)
            
        #We valid if type of message is chat message. Number 7. We execute the action corresponding       
        if typem == 7:
            longeurPseudo = struct.unpack_from('!B', datagram, 4)
            pseudo = struct.unpack_from('!'+ str(longeurPseudo[0]) + 's', datagram, 5)            
            longeurMessage = longueur - 5 - longeurPseudo[0]           
            message = struct.unpack_from('!'+ str(longeurMessage) + 's', datagram, 5 + longeurPseudo[0])
            pseudo = pseudo[0].decode('utf-8')
            message = message[0].decode('utf-8')            
            self.clientProxy.chatMessageReceivedONE(pseudo, message)
            
        #We valid if type of message is connexion refused. Number 9. We execute the action corresponding      
        if typem == 9:
            self.clientProxy.connectionRejectedONE("User name is already used, please try again!")
            #self.__init__(self.serverAddress, self.serverPort, self.clientProxy, self.lossPr)
            
        pass
    
    def headerGenerate(self, typem, longueur, num_sequence = 0):
        """
        :param integer typem: type of message.  
        :param integer longueur: message length including header + payload
        :param integer num_sequence: sequence number to differentiate each message of each client

        Called to generate the header message of any type of message.
        
        :returns a 4-byte integer that is generated with the three incoming parameters  
        """
        #We convert each integer (typem, longueur and num_sequence) to binary depending on bits number defined for each one.
        typeBin = '{0:04b}'.format(typem)
        num_sequenceBin = '{0:012b}'.format(self.num_sequence) if(typem != 0) else '{0:012b}'.format(num_sequence)
        longueurBin = '{0:016b}'.format(longueur)
 
        #We concatenate the three binary values to convert these to integer
        header = (int(typeBin + num_sequenceBin + longueurBin, 2))
                
        #We valide if type of message is not acquittement (0) by then to increase the sequence number
        if(typem != 0):
            self.fiabiliteClient[self.num_sequence] = [False,1,typem]            
            self.num_sequence += 1            

        return header
    

    def headerRead(self, header):
        """
        :param integer header: a 4-byte integer concerning with header.  

        Called to separate the header received in their components: type of message, sequence number, length.
        
        :returns three integers related to type of message, sequence number, length.  
        """
        #We convert the header to binary depending on bit numbers defined for it 
        headerBin = '{0:032b}'.format(header)
        
        #We separate the 32 bits tuple in 4 bits of type of message, 12 bits of sequence number, 16 bits of message length  
        typem, num_sequence, longueur = (int(headerBin[0:4], 2), 
                               int(headerBin[4:16], 2), 
                               int(headerBin[16:32], 2))

        return typem, num_sequence, longueur
    
    #Se hace el envio del acquittement 
    def sendAcquittement(self, num_sequenceEmetteur, host_port):        
        """
        :param integer num_sequenceEmetteur: the sequence number of message which will be acknowledged.
        :param host_port: a touple containing the source IP address and port.

        Called to send the acknowledgement
        """
        #We generate the header for acknowledge message
        header = self.headerGenerate(0, 4, num_sequenceEmetteur)
              
        buf = bytearray(4)
        struct.pack_into('!I', buf, 0, header)
        self.transport.write(buf, host_port)
        return
    
    def resendMessage(self, buf, host_port):
        """
        :param bytearray buf: it contains the complete message (header + payload) which will be resent.
        :param host_port: a touple containing the source IP address and port.

        Called to resend a message which wasn't received, it means that wasn't acknowledged after waiting 1 second
        """ 
        #We obtain sequence number of message to know if this was already acknowledged
        message_length = len(buf) - 4; 
        message = struct.unpack('!I' + str(message_length) + "s", buf)
        _, num_sequence, _ = self.headerRead(message[0]) 
        
        #If this message wasn't acknowledged, it will resend it until maximum 7 times
        if (self.fiabiliteClient[num_sequence][0] != True):
            if self.fiabiliteClient[num_sequence][1] < 8:
                self.transport.write(buf, host_port)
                reactor.callLater(1, self.resendMessage, buf, host_port)
                self.fiabiliteClient[num_sequence][1] += 1 
        return
    
    def userListRead(self,datagram):
        """
        :param string datagram: the payload and header of the UDP packet.

        Called to read the user list sent by the server.
        """ 
        #We create a list to save each user and we obtain the message length for having control over unpacking 
        listAux = []         
        header = struct.unpack_from('!I', datagram)
        _, _, maxLongueur = self.headerRead(header[0])
        count = 4
        #We valid if our counter is equal to message length. if it isn't equal, it must continue unpacking 
        while (count != maxLongueur):            
            headerUser = struct.unpack_from('!B', datagram, count)            
            userData = struct.unpack_from('!' + str(headerUser[0]) + 'sB', datagram, count + 1)            
            count = count + headerUser[0] + 2
            userName = userData[0].decode('utf-8')
            if (userData[1] == 0):
                listAux.append((userName, c2w.main.constants.ROOM_IDS.MAIN_ROOM))
            elif(userData[1] == 1):
                listAux.append((userName, c2w.main.constants.ROOM_IDS.MOVIE_ROOM))           
        self.userlist = listAux
        return
    
    def movieListRead(self,datagram):
        """
        :param string datagram: the payload and header of the UDP packet.

        Called to read the movie list sent by the server.
        """ 
        #We create a list to save each movie and we obtain the message length for having control over unpacking 
        listAux = [] 
        header = struct.unpack_from('!I', datagram)
        _, _, maxLongueur = self.headerRead(header[0])
        count = 4

        #We valid if our counter is equal to message length. if it isn't equal, it must continue unpacking         
        while (count != maxLongueur):            
            headermovie = struct.unpack_from('!B', datagram, count)            
            Data = struct.unpack_from('!' + str(headermovie[0]) + 'sBBBBH', datagram, count + 1) 
            titleMovie = Data[0].decode('utf-8')
            #We save movie name, movie IP (Date[1:5]) and movie Port (Data[5])
            listAux.append((titleMovie, str(Data[1]) + '.' + str(Data[2]) + '.' + str(Data[3]) + '.' + str(Data[4]), Data[5]))                      
            count = count + headermovie[0] + 7
        self.movielist = listAux       
        return
